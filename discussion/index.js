//[OBJECTIVE] Create a server-side app using Express Web Framework.

//Relate this task to something that you do on a daily basis.
/*
-->Let us append the entire app to our node package manager.
package.json -> the "heart" of every node project. This contains the metadata that describes the structure of the project.

scripts -> is used to declare and describe custom commands and keyword tha that can be used to execute this project with the correct runtime environment.

NODEMON -> is a utility that will monitor any changes in your source and automatically restart your server. It is like a live-server for your node application. Any changes made in your node applciation will get reflected automatically.

**NOTE: "start" is globally-recognized amongst node projects and frameworks as the 'default' command script to execute a task/project. 
		However, for *unconventional* keywords or commands, you have to add the command "run" at the start of the command.	
*/

//1. Identify and prepare the ingredients.
const express = require("express");
	//express => will be used as the main component to create the server.
	//we need to gather/acquire the utilities and components needed that the express library will provide us.
		//=> require() -> directive used to get the library/component needed inside the module.
		//Next step: prepare the environment in which the server will be served.

//[SECTION A.] Preparing a remote repository for our Node project.
	//NOTE: always DISABLE the node_modules folder
	//Why? (1) Because it will take up too much space in our repository, making it a lot more difficult to stage upon commiting the changes in our remote repository.
		// (2) If ever that you will deploy your node project on deployment platforms like (heroku, netlify, vercel) the project will automatically be rejected because node_modules is not recognized on various deployment platforms.
	//How? -> Using a .gitignore module


//[SECTION B.] Create a runtime environment that will automatically fix all the changes in our app.
	/*we're going to use a utility called nodemon. Upon starting the entry point module with nodemon, you will be able to 'append/execute' the application with the proper run time environment; allowing you to save time and effort upon committing changes to your app.*/
//You can even insert items like text art into your runtime environment. Link: https://fsymbols.com/text-art/

console.log(`Welcome to our Express API Server
─────────▒▒▒──────▒▒▒
────────▒▒───▒▒▒▒──▒░▒
───────▒▒───▒▒──▒▒──▒░▒
──────▒▒░▒──────▒▒──▒░▒
───────▒▒░▒────▒▒──▒░▒
─────────▒▒▒▒▒▒▒───▒▒
─────────────────▒▒▒
─────▒▒▒▒────────▒▒
───▒▒▒░░▒▒▒─────▒▒──▓▓▓▓▓▓▓▓
──▒▒─────▒▒▒────▒▒▓▓▓▓▓░░░░░▓▓──▓▓▓▓
─▒───▒▒────▒▒─▓▓▒░░░░░░░░░█▓▒▓▓▓▓░░▓▓▓
▒▒──▒─▒▒───▓▒▒░░▒░░░░░████▓▓▒▒▓░░░░░░▓▓
░▒▒───▒──▓▓▓░▒░░░░░░█████▓▓▒▒▒▒▓▓▓▓▓░░▓▓
──▒▒▒▒──▓▓░░░░░░███████▓▓▓▒▒▒▒▒▓───▓▓░▓▓
──────▓▓░░░░░░███████▓▓▓▒▒▒▒▒▒▒▓───▓░░▓▓
─────▓▓░░░░░███████▓▓▓▒▒▒▒▒▒▒▒▒▓▓▓▓░░▓▓
────▓▓░░░░██████▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▓░░░░▓▓
────▓▓▓░████▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓
─────▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓
─────▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓
──────▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓
───────▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓
─────────▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓
───────────▓▓▓▓▓▓▒▒▒▒▒▓▓▓▓
───────────────▓▓▓▓▓▓▓▓
`);
